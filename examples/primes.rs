use rust__::{int_main, ios::*, r#include};
macro_rules! include { ($($t:tt)*) => { rust__::include!($($t)*); }; }

r#include!(<cassert>);
r#include!(<cmath>);
r#include!(<cstdlib>);
r#include!(<filesystem>);
r#include!(<fstream>);
r#include!(<iostream>);
r#include!(<iterator>);
r#include!(<ranges>);
r#include!(<regex>);
r#include!(<string>);

int_main! {
    std::cout << "calculate primes until: " << std::flush;
    let mut max = (int) (0);
    std::cin >> &mut max;

    std::cout << "calculate primes using std::regex? " << std::flush;
    let mut regex = (int) (0);
    std::cin >> &mut regex;

    switch! { (max)
        case 1;
            std::cout << "1 is not prime" << std::endl;
            return 1;
        case 2;
        case 3;
            std::clog << "number needs to be above 3" << std::endl;
            return 2;
        default;
            if (max <= 0) {
                throw ("number needs to be positive");
            };
    };

    let p = fs::temp_directory_path() / "primes.txt";
    std::cout << "storing primes in " << &p << std::endl;

    let mut f = std::ofstream(p, out | trunc);
    if (!&f)
    {
        std::cerr << "could not create file" << std::endl;
        return 3;
    }

    let primes_regex = std::regex("^(..+)\\1+$");

    /*---------------------------*\
    |  checks if number is prime  |
    \*---------------------------*/
    let primes = views::closed_iota(2, max)
        | views::filter(|n: &int| {
            if (regex != 0)
            {
                let mut n = *n;
                let mut rep = (std::string)("");
                rep.reserve(n);
                while (0 <=*-- &mut n) {
                    rep.push_back(b'x');
                }
                return !std::regex_match(&rep, &primes_regex);
            }
            else
            {
                return views::iota(2, std::sqrt(n)) | views::none_of(|d: int| {
                    return n % d == 0;
                });
            }
        })
        | views::enumerate
        | views::transform(|(i, n)| {
            std::cout << "the " << (i+1) << ". prime is " << n << std::endl;
            return n;
        });

    let fi = std::ostream_iterator(&mut f, " ");
    primes | &fi;
    fi == std::endl;
    f.close();

    std::cout << "finished writing primes" << std::endl;
}
