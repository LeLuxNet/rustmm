use ::std::{
    ffi::{CStr, CString},
    fmt::{self, Display, Formatter},
    ops::{Add, AddAssign, Index},
};

use crate::{cast, namespace, regex::chars, size_t, std, stdexcept::out_of_range, throw};

namespace!(std = stringh {
    type string = string;
    fn string[I: Into<string>](s: I) -> string {
        s.into()
    }
});

#[doc(hidden)]
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct string {
    s: Vec<u8>,
}

impl string {
    pub fn at<I: cast<size_t>>(&self, pos: I) -> &u8 {
        let pos = pos.cast();
        if pos >= self.size() {
            throw(out_of_range {
                what: format!(
                    "basic_string::at: __n (which is {pos}) >= this->size() (which is {})",
                    self.size()
                ),
            });
        }
        &self[pos]
    }

    pub fn front(&self) -> &u8 {
        &self[0]
    }

    pub fn back(&self) -> &u8 {
        &self[self.size() - 1]
    }

    pub fn data(&self) -> &[u8] {
        &self.s
    }
    pub fn c_str(&self) -> &CStr {
        CStr::from_bytes_until_nul(&self.s).unwrap()
    }

    pub fn size(&self) -> size_t {
        (size_t)(self.s.len()) - 1
    }
    pub fn length(&self) -> size_t {
        self.size()
    }

    pub const fn max_size(&self) -> size_t {
        let max = usize::MAX / size_of::<u8>() - 1;
        size_t { n: max }
    }

    pub fn empty(&self) -> bool {
        self.size() == 0
    }

    pub fn capacity(&self) -> size_t {
        (size_t)(self.s.capacity()) - 1
    }

    pub fn reserve<N: cast<size_t>>(&mut self, n: N) {
        if let Some(additional) = n.cast().n.checked_sub(self.size().n) {
            self.s.reserve_exact(additional);
        }
    }

    pub fn shrink_to_fit(&mut self) {
        self.s.shrink_to_fit();
    }

    pub fn clear(&mut self) {
        self.s.clear();
        self.s.push(0);
    }

    pub fn push_back(&mut self, c: u8) {
        self.s.insert(self.size().n, c);
    }
}

impl From<char> for string {
    fn from(value: char) -> Self {
        let mut s = value.to_string().into_bytes();
        s.push(0);
        string { s }
    }
}

impl From<CString> for string {
    fn from(value: CString) -> Self {
        string {
            s: value.into_bytes(),
        }
    }
}

impl From<String> for string {
    fn from(value: String) -> Self {
        let mut s = value.into_bytes();
        s.push(0);
        string { s }
    }
}

impl From<&str> for string {
    fn from(value: &str) -> Self {
        value.to_owned().into()
    }
}

impl Add<&str> for string {
    type Output = Self;
    fn add(mut self, rhs: &str) -> string {
        self += rhs;
        self
    }
}

impl AddAssign<&str> for string {
    fn add_assign(&mut self, rhs: &str) {
        self.s.pop();
        self.s.extend_from_slice(rhs.as_bytes());
        self.s.push(0);
    }
}

impl<T: cast<size_t>> Index<T> for string {
    type Output = u8;
    fn index(&self, index: T) -> &Self::Output {
        &self.s[index.cast().n]
    }
}

impl Display for string {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.as_str())
    }
}
