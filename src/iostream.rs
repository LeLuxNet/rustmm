use ::std::{
    fmt,
    io::{self, stderr, stdin, stdout, IoSlice, IoSliceMut, Read, Write},
    ops::Shr,
    str::FromStr,
};

use crate::{istream, namespace, ostream::ostream, std};

namespace!(std: <ostream> = iostreamh {
    /// ```
    /// # use rust__::{r#include, std};
    /// r#include!(<iostream>);
    ///
    /// let mut count = 0;
    /// std::cin >> &mut count;
    /// std::cout << "Your answer is " << count << std::endl;
    /// ```
    const cin: istream<Cin> = istream::new(Cin);

    /// ```
    /// # use rust__::{r#include, std};
    /// r#include!(<iostream>);
    ///
    /// std::cout << "hello world" << std::endl;
    /// ```
    /// will output
    /// ```text
    /// hello world
    ///
    /// ```
    const cout: ostream<Cout> = ostream::new(Cout);

    const cerr: ostream<Cerr> = ostream::new(Cerr);
    const clog: ostream<Cerr> = ostream::new(Cerr);
});

#[doc(hidden)]
pub struct Cin;

impl Read for Cin {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        stdin().read(buf)
    }
    fn read_vectored(&mut self, bufs: &mut [IoSliceMut<'_>]) -> io::Result<usize> {
        stdin().read_vectored(bufs)
    }
    fn read_to_end(&mut self, buf: &mut Vec<u8>) -> io::Result<usize> {
        stdin().read_to_end(buf)
    }
    fn read_to_string(&mut self, buf: &mut String) -> io::Result<usize> {
        stdin().read_to_string(buf)
    }
    fn read_exact(&mut self, buf: &mut [u8]) -> io::Result<()> {
        stdin().read_exact(buf)
    }
}

impl<V: FromStr + Default> Shr<&mut V> for &mut istream<Cin> {
    type Output = Self;
    fn shr(self, rhs: &mut V) -> Self::Output {
        let mut s = istream::new(stdin().lock());
        _ = &mut s >> rhs;
        self.b = s.b;
        self
    }
}
impl<V: FromStr + Default> Shr<&mut V> for istream<Cin> {
    type Output = Self;
    fn shr(mut self, rhs: &mut V) -> Self::Output {
        _ = &mut self >> rhs;
        self
    }
}

#[doc(hidden)]
pub struct Cout;

impl Write for Cout {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        stdout().write(buf)
    }
    fn write_vectored(&mut self, bufs: &[IoSlice<'_>]) -> io::Result<usize> {
        stdout().write_vectored(bufs)
    }
    fn flush(&mut self) -> io::Result<()> {
        stdout().flush()
    }
    fn write_all(&mut self, buf: &[u8]) -> io::Result<()> {
        stdout().write_all(buf)
    }
    fn write_fmt(&mut self, fmt: fmt::Arguments<'_>) -> io::Result<()> {
        stdout().write_fmt(fmt)
    }
}

#[doc(hidden)]
pub struct Cerr;

impl Write for Cerr {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        stderr().write(buf)
    }
    fn write_vectored(&mut self, bufs: &[IoSlice<'_>]) -> io::Result<usize> {
        stderr().write_vectored(bufs)
    }
    fn flush(&mut self) -> io::Result<()> {
        stderr().flush()
    }
    fn write_all(&mut self, buf: &[u8]) -> io::Result<()> {
        stderr().write_all(buf)
    }
    fn write_fmt(&mut self, fmt: fmt::Arguments<'_>) -> io::Result<()> {
        stderr().write_fmt(fmt)
    }
}
