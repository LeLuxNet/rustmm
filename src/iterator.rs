use ::std::{fmt::Display, io::Write, marker::PhantomData, sync::Mutex};

use crate::{namespace, ostream::ostream, ranges::adaptor, std};

namespace!(std = iteratorh {
    fn ostream_iterator['a, T: Display](
        os: &'a mut ostream<impl Write>,
        sep: &'a str,
    ) -> ostream_iterator<'a, T, impl Write> {
        ostream_iterator {
            t: PhantomData,
            os: Mutex::new(os),
            sep,
        }
    }
});

#[doc(hidden)]
pub struct ostream_iterator<'a, T, W> {
    t: PhantomData<T>,
    os: Mutex<&'a mut ostream<W>>,
    sep: &'a str,
}

impl<'a, I, W: Write> adaptor<I> for &ostream_iterator<'a, I::Item, W>
where
    I: Iterator,
    I::Item: Display,
{
    type Output = ();

    fn pipe(self, i: I) -> Self::Output {
        let mut w = self.os.lock().unwrap();
        for e in i {
            let _ = write!(w.w, "{}{}", e, self.sep);
        }
    }
}

impl<'a, T, D: Display, W: Write> PartialEq<D> for ostream_iterator<'a, T, W> {
    fn eq(&self, other: &D) -> bool {
        let mut w = self.os.lock().unwrap();
        let _ = write!(w.w, "{}{}", other, self.sep);
        false
    }

    #[allow(clippy::partialeq_ne_impl)]
    fn ne(&self, _: &D) -> bool {
        true
    }
}
