use crate::{cast, double, float, namespace, std};

namespace!(std = cmathh {
    /// ```
    /// # use rust__::{r#include, std, double};
    /// r#include!(<cassert>);
    /// r#include!(<cmath>);
    ///
    /// std::assert(std::sqrt(9) == 3.0);
    /// ```
    fn sqrt(num: impl cast<double>) -> double {
        (double) (num.cast().sqrt())
    }
    fn sqrtf(num: float) -> float {
        (float) (num.sqrt())
    }

    /// ```
    /// # use rust__::{r#include, std, double};
    /// r#include!(<cassert>);
    /// r#include!(<cmath>);
    ///
    /// std::assert(std::isnan(0.0 / 0.0));
    /// ```
    fn isnan(num: impl cast<double>) -> bool {
        num.cast().is_nan()
    }
});
