use std::{
    cmp::Ordering,
    ffi::{c_char, c_double, c_float, c_int, c_long, c_longlong, c_schar, c_uchar},
    fmt::{self, Display, Formatter},
    ops::{
        Add, AddAssign, Deref, DerefMut, Div, DivAssign, Mul, MulAssign, Neg, Rem, RemAssign, Sub,
        SubAssign,
    },
    str::FromStr,
};

macro_rules! number_ops {
    ($name:ident : $type:ty => $op:ident $opfn:ident, $opassign:ident $opassignfn:ident) => {
        impl<N> $op<N> for $name
        where
            $type: $op<N, Output = $type>,
        {
            type Output = Self;
            fn $opfn(self, rhs: N) -> Self::Output {
                Self {
                    n: self.n.$opfn(rhs),
                }
            }
        }

        impl $op<$name> for &$name {
            type Output = $name;
            fn $opfn(self, rhs: $name) -> Self::Output {
                $name {
                    n: self.n.$opfn(rhs.n),
                }
            }
        }

        impl<N> $op<N> for &$name
        where
            $type: $op<N, Output = $type>,
        {
            type Output = $name;
            fn $opfn(self, rhs: N) -> Self::Output {
                $name {
                    n: self.n.$opfn(rhs),
                }
            }
        }

        impl $op<$name> for $type {
            type Output = $name;
            fn $opfn(self, rhs: $name) -> Self::Output {
                $name {
                    n: self.$opfn(rhs.n),
                }
            }
        }

        impl $opassign for $name {
            fn $opassignfn(&mut self, rhs: Self) {
                self.n.$opassignfn(rhs.n);
            }
        }

        impl<N> $opassign<N> for $name
        where
            $type: $opassign<N>,
        {
            fn $opassignfn(&mut self, rhs: N) {
                self.n.$opassignfn(rhs);
            }
        }
    };
}

macro_rules! number {
    ($name:ident : $type:ty) => {
        number!($name : $type, $type);
    };
    ($name:ident : $type:ty, $stype:ty) => {
        number!($name (Eq, Ord, Hash) : $type, $stype);
    };
    ($name:ident? : $type:ty) => {
        number!($name? : $type, $type);
    };
    ($name:ident? : $type:ty, $stype:ty) => {
        number!($name () : $type, $stype);
    };
    ($name:ident ($($derive:path),*) : $type:ty, $stype:ty) => {
        pub fn $name<N: cast<$name>>(n: N) -> $name {
            n.cast()
        }

        #[derive(Debug, Default, Clone, Copy, $($derive),* )]
        pub struct $name {
            #[doc(hidden)]
            pub n: $type,
        }

        impl cast<$name> for &$type {
            fn cast(self) -> $name {
                $name { n: *self }
            }
        }

        impl cast<$type> for $name {
            fn cast(self) -> $type {
                self.n
            }
        }

        impl cast<$type> for &$name {
            fn cast(self) -> $type {
                self.n
            }
        }

        impl cast<double> for $name {
            fn cast(self) -> double {
                double { n: self.n.cast() }
            }
        }
        impl cast<double> for &$name {
            fn cast(self) -> double {
                double { n: self.n.cast() }
            }
        }

        impl cast<int> for $name {
            fn cast(self) -> int {
                int { n: self.n.cast() }
            }
        }
        impl cast<int> for &$name {
            fn cast(self) -> int {
                int { n: self.n.cast() }
            }
        }

        impl cast<size_t> for $name {
            fn cast(self) -> size_t {
                size_t { n: self.n.cast() }
            }
        }
        impl cast<size_t> for &$name {
            fn cast(self) -> size_t {
                size_t { n: self.n.cast() }
            }
        }

        impl cast<$name> for bool {
            fn cast(self) -> $name {
                (self as u8).cast()
            }
        }
        impl cast<$name> for u8 {
            fn cast(self) -> $name {
                $name { n: self.cast() }
            }
        }
        impl cast<$name> for u16 {
            fn cast(self) -> $name {
                $name { n: self.cast() }
            }
        }
        impl cast<$name> for u32 {
            fn cast(self) -> $name {
                $name { n: self.cast() }
            }
        }
        impl cast<$name> for u64 {
            fn cast(self) -> $name {
                $name { n: self.cast() }
            }
        }
        impl cast<$name> for usize {
            fn cast(self) -> $name {
                $name { n: self.cast() }
            }
        }
        impl cast<$name> for i8 {
            fn cast(self) -> $name {
                $name { n: self.cast() }
            }
        }
        impl cast<$name> for i16 {
            fn cast(self) -> $name {
                $name { n: self.cast() }
            }
        }
        impl cast<$name> for i32 {
            fn cast(self) -> $name {
                $name { n: self.cast() }
            }
        }
        impl cast<$name> for i64 {
            fn cast(self) -> $name {
                $name { n: self.cast() }
            }
        }
        impl cast<$name> for isize {
            fn cast(self) -> $name {
                $name { n: self.cast() }
            }
        }
        impl cast<$name> for f32 {
            fn cast(self) -> $name {
                $name { n: self.cast() }
            }
        }
        impl cast<$name> for f64 {
            fn cast(self) -> $name {
                $name { n: self.cast() }
            }
        }

        impl Deref for $name {
            type Target = $type;
            fn deref(&self) -> &Self::Target {
                &self.n
            }
        }

        impl DerefMut for $name {
            fn deref_mut(&mut self) -> &mut Self::Target {
                &mut self.n
            }
        }

        impl Display for $name {
            fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
                write!(f, "{}", self.n)
            }
        }

        impl FromStr for $name {
            type Err = <$type as FromStr>::Err;
            fn from_str(s: &str) -> Result<Self, Self::Err> {
                <$type>::from_str(s).map($name)
            }
        }

        impl<N> PartialEq<N> for $name
        where
            $type: PartialEq<N>
        {
            fn eq(&self, rhs: &N) -> bool {
                self.n.eq(rhs)
            }
        }

        impl PartialEq<$name> for $type {
            fn eq(&self, rhs: &$name) -> bool {
                self.eq(&rhs.n)
            }
        }

        impl<N> PartialOrd<N> for $name
        where
            $type: PartialOrd<N>
        {
            fn partial_cmp(&self, rhs: &N) -> Option<Ordering> {
                self.n.partial_cmp(rhs)
            }
        }

        impl PartialOrd<$name> for $type {
            fn partial_cmp(&self, rhs: &$name) -> Option<Ordering> {
                self.partial_cmp(&rhs.n)
            }
        }

        number_ops!($name: $type => Add add, AddAssign add_assign);
        number_ops!($name: $type => Sub sub, SubAssign sub_assign);
        number_ops!($name: $type => Mul mul, MulAssign mul_assign);
        number_ops!($name: $type => Div div, DivAssign div_assign);
        number_ops!($name: $type => Rem rem, RemAssign rem_assign);

        impl Neg for $name {
            type Output = dec<$name>;
            fn neg(mut self) -> Self::Output {
                self.n = -(self.n as $stype) as $type;
                dec(self)
            }
        }

        impl Dec for $name {
            type Output = $name;
            fn dec(mut self) -> Self::Output {
                self.n = -(self.n as $stype) as $type - 1 as $type;
                self
            }
        }

        impl<'a> Neg for &'a mut $name {
            type Output = dec<&'a mut $name>;
            fn neg(self) -> Self::Output {
                self.n = -(self.n as $stype) as $type;
                dec(self)
            }
        }

        impl<'a> Dec for &'a mut $name {
            type Output = &'a mut $name;
            fn dec(self) -> Self::Output {
                self.n = -(self.n as $stype) as $type - 1 as $type;
                self
            }
        }
    };
}

number!(char: c_char, c_schar);
number!(signed_char: c_schar);
number!(unsigned_char: c_uchar, c_schar);
number!(int: c_int);
number!(long: c_long);
number!(long_long: c_longlong);
number!(float?: c_float);
number!(double?: c_double);

number!(int8_t: i8);
number!(int16_t: i16);
number!(int32_t: i32);
number!(int64_t: i64);

number!(uint8_t: u8, i8);
number!(uint16_t: u16, i16);
number!(uint32_t: u32, i32);
number!(uint64_t: u64, i64);

number!(size_t: usize, isize);
number!(uintmax_t: u64, i64);

number!(streamsize: isize);

#[doc(hidden)]
pub trait cast<T> {
    fn cast(self) -> T;
}

macro_rules! cast {
    ({} { $($from:tt)* }) => {};
    ({ $for:ident: $via:ident, $($forrest:tt)* } { $($from:ident: $fromvia:ident,)* }) => {
        $(impl cast<$for> for $from {
            fn cast(self) -> $for {
                self as $for
            }
        })+
        cast!({ $($forrest)* } { $($from: $fromvia,)+ });
    };
    ( $($type:tt)+ ) => {
        cast!({ $($type)+ } { $($type)+ });
    };
}

cast!(
    u8: u8, u16: u16, u32: u32, u64: u64, u128: u128, usize: usize,
    i8: i8, i16: i16, i32: i32, i64: i64, i128: i128, isize: isize,
    f32: f32, f64: f64,
);

#[doc(hidden)]
pub trait Dec {
    type Output;
    fn dec(self) -> Self::Output;
}

#[doc(hidden)]
pub struct dec<T>(T);

impl<T: Dec> Neg for dec<T> {
    type Output = T::Output;

    fn neg(self) -> Self::Output {
        self.0.dec()
    }
}

impl<T> Deref for dec<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T> DerefMut for dec<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl<T: Display> Display for dec<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}
