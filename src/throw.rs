use std::{
    any::Any,
    panic::{catch_unwind, panic_any, resume_unwind, AssertUnwindSafe},
    process::abort,
};

use crate::{exception::exception, regex::regex_error, stdexcept::out_of_range};

/// ```
/// # use rust__::{int_main, r#include};
/// r#include!(<iostream>);
///
/// int_main! {
///     if (1 + 1 != 3) {
///         return 1;
///     }
///
///     std::cout << "Hello world";
/// }
/// ```
#[macro_export]
macro_rules! int_main {
    ($($t:tt)*) => {
        use $crate::*;

        #[allow(unused_imports, unused_parens, needless_return, const_item_mutation, clippy::double_neg)]
        #[warn(clippy::implicit_return)]
        fn main() {
            ::std::panic::set_hook(Box::new(|_| {}));

            #[allow(unreachable_code, unused_must_use)]
            let f = || {
                { $($t)* };
                return 0;
            };

            if let Err(e) = ::std::panic::catch_unwind(|| {
                let res = f();
                if res != 0 {
                    ::std::process::exit(res);
                };
            }) {
                $crate::main_unwind(e);
            };
        }
    };
}

#[doc(hidden)]
pub fn main_unwind(e: Box<dyn Any>) {
    let mut exception: Option<(&str, &dyn exception)> = None;

    if let Some(e) = e.downcast_ref::<out_of_range>() {
        exception = Some(("std::out_of_range", e));
    } else if let Some(e) = e.downcast_ref::<regex_error>() {
        exception = Some(("std::regex_error", e));
    }

    if let Some((name, exception)) = exception {
        eprintln!("terminate called after throwing an instance of '{name}'");
        eprintln!("  what():  {}", exception.what());
    } else if let Some(s) = e.downcast_ref::<&str>() {
        eprintln!("terminate called after throwing: {s}");
    } else {
        eprintln!("terminate called after throwing");
    }
    abort();
}

#[doc(hidden)]
#[macro_export]
macro_rules! blockless {
    ({$($comp:tt)*} if ($cond:expr) $then:stmt; else $else:stmt; $($rest:tt)*) => {
        blockless!{{
            $($comp)*
            if $cond {
                {$then};
            } else {
                {$else};
            }
        } $($rest)*}
    };
    ({$($comp:tt)*} if ($cond:expr) $then:stmt; $($rest:tt)*) => {
        blockless!{{
            $($comp)*
            if $cond {
                {$then};
            }
        } $($rest)*}
    };
    ({$($comp:tt)*} $first:tt $($rest:tt)*) => {
        blockless!{{
            $($comp)*
            $first
        } $($rest)*}
    };
    ({$($comp:tt)*}) => {
        $($comp)*
    };
}

pub fn r#try(f: impl FnOnce()) -> Result<(), Box<dyn Any + Send + 'static>> {
    catch_unwind(AssertUnwindSafe(f))
}

pub fn throw(e: impl Any + 'static + Send) -> ! {
    panic_any(e);
}

#[doc(hidden)]
pub trait CatchExt {
    fn catch<T: 'static>(self, f: impl FnOnce(T)) -> Self;
    fn throw(self);
}

#[doc(hidden)]
impl CatchExt for Result<(), Box<dyn Any + Send + 'static>> {
    fn catch<T: 'static>(self, f: impl FnOnce(T)) -> Self {
        let Err(e) = self else {
            return self;
        };

        match e.downcast() {
            Ok(b) => {
                f(*b);
                Ok(())
            }
            Err(e) => Err(e),
        }
    }

    fn throw(self) {
        if let Err(e) = self {
            resume_unwind(e)
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{r#try, throw, CatchExt};

    #[test]
    fn catch() {
        r#try(|| {
            throw(5);
        })
        .catch(|v: i32| {
            assert_eq!(v, 5);
        })
        .unwrap();
    }
}
