use ::std::io::Cursor;

use crate::{istream, namespace, std};

namespace!(std = sstreamh {
    fn istringstream['a, I: Into<&'a str>](s: I) -> istream<Cursor<&'a [u8]>> {
        let s: &str = s.into();
        istream::new(Cursor::new(s.as_bytes()))
    }
});
