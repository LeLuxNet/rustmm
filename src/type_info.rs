use ::std::{
    any::{type_name, TypeId},
    collections::HashMap,
    sync::{LazyLock, Mutex},
};

use crate::{cast, namespace, size_t, std};

namespace!(std = type_infoh {
    fn type_info[T: ?Sized + 'static]() -> type_info {
        let id = TypeId::of::<T>();
        let hash_code = {
            let mut lock = TYPE_IDS.lock().unwrap();
            let len = lock.len().cast();
            *lock.entry(id).or_insert(len)
        };

        type_info {
            hash_code,
            name: type_name::<T>(),
        }
    }
});

#[derive(PartialEq, Eq)]
pub struct type_info {
    hash_code: size_t,
    name: &'static str,
}

static TYPE_IDS: LazyLock<Mutex<HashMap<TypeId, size_t>>> = LazyLock::new(Mutex::default);

impl type_info {
    pub const fn hash_code(self) -> size_t {
        self.hash_code
    }

    pub const fn name(self) -> &'static str {
        self.name
    }
}

#[cfg(test)]
mod tests {
    use crate::{cassert::h, float, int32_t, r#include, std};

    r#include!(<cassert>);
    r#include!(<type_info>);

    #[test]
    fn unique_hash_code() {
        let int_code = std::type_info::<int32_t>().hash_code();
        let float_code = std::type_info::<float>().hash_code();
        let int_code2 = std::type_info::<int32_t>().hash_code();
        let i32_code = std::type_info::<i32>().hash_code();

        std::assert(int_code == int_code2);
        std::assert(int_code != float_code);
        std::assert(int_code != i32_code);
    }
}
