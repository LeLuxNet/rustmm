use ::std::process::{abort, exit, Command};

#[cfg(unix)]
use ::std::os::unix::process::CommandExt;

#[cfg(target_os = "windows")]
use ::std::{env, ffi::OsStr};

use crate::{cast, int, namespace, std};

namespace!(std = cstdlibh {
    fn abort() -> ! {
        abort();
    }

    fn exit(code: int) -> ! {
        exit(code.cast());
    }

    /// ```
    /// # use rust__::{r#include, std};
    /// r#include!(<cassert>);
    /// r#include!(<cstdlib>);
    ///
    /// std::system("echo Hello!");
    /// std::assert(std::system("exit 23") == 23);
    /// ```
    fn system(s: &str) -> int {
        #[cfg(unix)]
        let res = Command::new("/bin/sh")
            .arg0("sh")
            .arg("-c")
            .arg("--")
            .arg(s)
            .status();

        #[cfg(target_os = "windows")]
        let comspec = env::var_os("COMSPEC");
        #[cfg(target_os = "windows")]
        let res = Command::new(comspec.as_deref().unwrap_or(OsStr::new("cmd.exe")))
            .arg("/c")
            .arg(s)
            .status();

        #[cfg(not(any(unix, target_os = "windows")))]
        panic!("unknown os");

        let code = res.unwrap().code().unwrap();
        return (int) (code);
    }
});
