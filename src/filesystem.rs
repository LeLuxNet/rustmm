use std::{
    env::{current_dir, temp_dir},
    ffi::OsStr,
    fmt::{self, Debug, Display, Formatter},
    fs::remove_file,
    io::ErrorKind,
    mem::swap,
    ops::{AddAssign, Div, DivAssign},
    path::{Path, PathBuf},
};

#[cfg(unix)]
use std::os::unix::fs::FileTypeExt;

use crate::{namespace, throw, uintmax_t};

pub(crate) mod fs {
    pub struct filesystem;
}

namespace!(fs::filesystem = filesystemh {
    fn absolute(p: &path) -> path {
        current_dir().unwrap().join(p).into()
    }

    fn file_size(p: &path) -> uintmax_t {
        (uintmax_t) (p.0.metadata().unwrap().len())
    }

    fn current_path() -> path {
        current_dir().unwrap().into()
    }

    fn exists(p: &path) -> bool {
        p.0.exists()
    }

    fn read_symlink(p: &path) -> path {
        path(p.0.read_link().unwrap())
    }

    /// ```
    /// # use rust__::{r#include, std, fs, ios::*};
    /// r#include!(<cassert>);
    /// r#include!(<filesystem>);
    /// r#include!(<fstream>);
    ///
    /// let p = fs::temp_directory_path() / "remove.txt";
    /// std::ofstream(&p, out | trunc).close();
    ///
    /// std::assert(fs::remove(&p) == true); // file deleted
    /// std::assert(fs::remove(&p) == false); // file did not exist anymore
    /// ```
    fn remove(p: &path) -> bool {
        if let Err(e) = remove_file(p) {
            if e.kind() == ErrorKind::NotFound {
                false
            } else {
                throw (e);
            }
        } else {
            true
        }
    }

    fn temp_directory_path() -> path {
        temp_dir().into()
    }

    fn is_block_file(p: &path) -> bool {
        #[cfg(unix)]
        { p.0.metadata().unwrap().file_type().is_block_device() }
        #[cfg(not(unix))]
        false
    }
    fn is_character_file(p: &path) -> bool {
        #[cfg(unix)]
        { p.0.metadata().unwrap().file_type().is_char_device() }
        #[cfg(not(unix))]
        false
    }
    fn is_directory(p: &path) -> bool {
        p.0.is_dir()
    }
    fn is_empty(p: &path) -> bool {
        let meta = p.0.metadata().unwrap();
        if meta.is_file() {
            meta.len() == 0
        } else {
            p.0.read_dir().unwrap().next().is_none()
        }
    }
    fn is_fifo(p: &path) -> bool {
        #[cfg(unix)]
        { p.0.metadata().unwrap().file_type().is_fifo() }
        #[cfg(not(unix))]
        false
    }
    fn is_socket(p: &path) -> bool {
        #[cfg(unix)]
        { p.0.metadata().unwrap().file_type().is_socket() }
        #[cfg(not(unix))]
        false
    }
    fn is_symlink(p: &path) -> bool {
        p.0.is_symlink()
    }

    type path = path;
    fn path(p: impl Into<PathBuf>) -> path {
        path(p.into())
    }
});

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct path(PathBuf);

impl path {
    pub const preferred_separator: &'static str = std::path::MAIN_SEPARATOR_STR;

    pub fn clear(&mut self) {
        self.0.clear();
    }

    pub fn empty(&self) -> bool {
        self.0.as_os_str().is_empty()
    }

    pub fn swap(&mut self, other: &mut Self) {
        swap(&mut self.0, &mut other.0)
    }

    pub fn is_absolute(&self) -> bool {
        self.0.is_absolute()
    }

    pub fn is_relative(&self) -> bool {
        self.0.is_relative()
    }
}

impl AsRef<Path> for path {
    fn as_ref(&self) -> &Path {
        self.0.as_ref()
    }
}

impl<P: Into<PathBuf>> From<P> for path {
    fn from(value: P) -> Self {
        path(value.into())
    }
}

impl Display for path {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0.display())
    }
}

impl Debug for path {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self.0.display())
    }
}

impl<P: AsRef<Path>> Div<P> for path {
    type Output = path;

    fn div(mut self, rhs: P) -> Self::Output {
        self.0.push(rhs);
        self
    }
}

impl<P: AsRef<Path>> Div<P> for &path {
    type Output = path;

    fn div(self, rhs: P) -> Self::Output {
        path(self.0.join(rhs))
    }
}

impl<P: AsRef<Path>> DivAssign<P> for path {
    fn div_assign(&mut self, rhs: P) {
        self.0.push(rhs);
    }
}

impl<P: AsRef<OsStr>> AddAssign<P> for path {
    fn add_assign(&mut self, rhs: P) {
        self.0.as_mut_os_string().push(rhs);
    }
}
