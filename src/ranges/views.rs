use std::{
    iter::{self, Enumerate, Filter, Flatten, Map, Rev, Skip, SkipWhile, Take, TakeWhile, Zip},
    marker::PhantomData,
    ops::AddAssign,
    vec::IntoIter,
};

use crate::{
    cast, namespace,
    ranges::{adaptor, range},
};

pub struct views;

namespace!(views {
    /// ```
    /// # use rust__::{include, views, int, vector::vector};
    /// r#include!(<ranges>);
    /// r#include!(<vector>);
    ///
    /// let v: vector<int> = views::iota(0, 5) | views::repeat(2) | views::to();
    /// assert_eq!(v.size(), 10);
    /// ```
    class repeat(n: usize);

    class filter<F>(f: F);

    class transform<F>(f: F);

    class take(n: usize);
    class take_while<F>(f: F);

    class drop(n: usize);
    class drop_while<F>(f: F);

    const join: join = join;
    const reverse: reverse = reverse;
    const enumerate: enumerate = enumerate;

    class zip<J>(other: J);

    const sort: sort = sort;
    const stable_sort: stable_sort = stable_sort;

    class all_of<F>(f: F);
    class any_of<F>(f: F);
    class none_of<F>(f: F);

    class for_each<F>(f: F);

    fn to[T]() -> to<T> {
        to::to
    }

    fn iota[N: AddAssign + PartialOrd + Clone](from: impl cast<N>, to: impl cast<N>) -> range<iota<N>> {
        range(iota {
            from: from.cast(),
            to: Some(to.cast()),
            closed: false,
        })
    }

    fn closed_iota[N: AddAssign + PartialOrd + Clone](from: impl cast<N>, to: impl cast<N>) -> range<iota<N>> {
        range(iota {
            from: from.cast(),
            to: Some(to.cast()),
            closed: true,
        })
    }

    fn copy[I, S: IntoIterator<Item = I> + Clone](
        source: &S,
        destination: impl adaptor<S::IntoIter>,
    ) {
        destination.pipe(source.clone().into_iter());
    }
});

#[doc(hidden)]
pub struct repeat(usize);
impl<I> adaptor<I> for repeat
where
    I: Iterator + Clone,
{
    type Output = range<Flatten<Take<iter::Repeat<I>>>>;
    fn pipe(self, i: I) -> Self::Output {
        range(iter::repeat(i).take(self.0).flatten())
    }
}

#[doc(hidden)]
pub struct filter<F>(F);
impl<I, F> adaptor<I> for filter<F>
where
    I: Iterator,
    F: FnMut(&I::Item) -> bool,
{
    type Output = range<Filter<I, F>>;
    fn pipe(self, i: I) -> Self::Output {
        range(i.filter(self.0))
    }
}

#[doc(hidden)]
pub struct transform<F>(pub F);
impl<I, F, O> adaptor<I> for transform<F>
where
    I: Iterator,
    F: FnMut(I::Item) -> O,
{
    type Output = range<Map<I, F>>;
    fn pipe(self, i: I) -> Self::Output {
        range(i.map(self.0))
    }
}

#[doc(hidden)]
pub struct take(usize);
impl<I> adaptor<I> for take
where
    I: Iterator,
{
    type Output = range<Take<I>>;
    fn pipe(self, i: I) -> Self::Output {
        range(i.take(self.0))
    }
}

#[doc(hidden)]
pub struct take_while<F>(F);
impl<I, F> adaptor<I> for take_while<F>
where
    I: Iterator,
    F: FnMut(&I::Item) -> bool,
{
    type Output = range<TakeWhile<I, F>>;
    fn pipe(self, i: I) -> Self::Output {
        range(i.take_while(self.0))
    }
}

#[doc(hidden)]
pub struct drop(usize);
impl<I> adaptor<I> for drop
where
    I: Iterator,
{
    type Output = range<Skip<I>>;
    fn pipe(self, i: I) -> Self::Output {
        range(i.skip(self.0))
    }
}

#[doc(hidden)]
pub struct drop_while<F>(pub F);
impl<I, F> adaptor<I> for drop_while<F>
where
    I: Iterator,
    F: FnMut(&I::Item) -> bool,
{
    type Output = range<SkipWhile<I, F>>;
    fn pipe(self, i: I) -> Self::Output {
        range(i.skip_while(self.0))
    }
}

#[doc(hidden)]
pub struct join;
impl<I> adaptor<I> for join
where
    I: Iterator,
    I::Item: Iterator,
{
    type Output = range<Flatten<I>>;
    fn pipe(self, i: I) -> Self::Output {
        range(i.flatten())
    }
}

#[doc(hidden)]
pub struct reverse;
impl<I> adaptor<I> for reverse
where
    I: DoubleEndedIterator,
{
    type Output = range<Rev<I>>;
    fn pipe(self, i: I) -> Self::Output {
        range(i.rev())
    }
}

#[doc(hidden)]
pub struct enumerate;
impl<I> adaptor<I> for enumerate
where
    I: Iterator,
{
    type Output = range<Enumerate<I>>;
    fn pipe(self, i: I) -> Self::Output {
        range(i.enumerate())
    }
}

#[doc(hidden)]
pub struct zip<J>(J);
impl<I, J> adaptor<I> for zip<J>
where
    I: Iterator,
    J: IntoIterator,
{
    type Output = range<Zip<I, J::IntoIter>>;
    fn pipe(self, i: I) -> Self::Output {
        range(i.zip(self.0))
    }
}

#[doc(hidden)]
pub struct sort;
impl<I> adaptor<I> for sort
where
    I: Iterator,
    I::Item: Ord,
{
    type Output = range<IntoIter<I::Item>>;
    fn pipe(self, i: I) -> Self::Output {
        let mut v: Vec<_> = i.collect();
        v.sort_unstable();
        range(v.into_iter())
    }
}

#[doc(hidden)]
pub struct stable_sort;
impl<I> adaptor<I> for stable_sort
where
    I: Iterator,
    I::Item: Ord,
{
    type Output = range<IntoIter<I::Item>>;
    fn pipe(self, i: I) -> Self::Output {
        let mut v: Vec<_> = i.collect();
        v.sort();
        range(v.into_iter())
    }
}

#[doc(hidden)]
pub struct all_of<F>(F);
impl<I, F> adaptor<I> for all_of<F>
where
    I: Iterator,
    F: FnMut(I::Item) -> bool,
{
    type Output = bool;
    fn pipe(self, mut i: I) -> Self::Output {
        i.all(self.0)
    }
}

#[doc(hidden)]
pub struct any_of<F>(F);
impl<I, F> adaptor<I> for any_of<F>
where
    I: Iterator,
    F: FnMut(I::Item) -> bool,
{
    type Output = bool;
    fn pipe(self, mut i: I) -> Self::Output {
        i.any(self.0)
    }
}

#[doc(hidden)]
pub struct none_of<F>(F);
impl<I, F> adaptor<I> for none_of<F>
where
    I: Iterator,
    F: FnMut(I::Item) -> bool,
{
    type Output = bool;
    fn pipe(self, mut i: I) -> Self::Output {
        !i.any(self.0)
    }
}

#[doc(hidden)]
pub struct for_each<F>(F);
impl<I, F> adaptor<I> for for_each<F>
where
    I: Iterator,
    F: FnMut(I::Item),
{
    type Output = ();
    fn pipe(self, i: I) -> Self::Output {
        i.for_each(self.0)
    }
}

#[doc(hidden)]
pub enum to<T> {
    #[doc(hidden)]
    _phantom(PhantomData<T>),
    to,
}

impl<I, T> adaptor<I> for to<T>
where
    I: Iterator,
    T: FromIterator<I::Item>,
{
    type Output = T;
    fn pipe(self, i: I) -> Self::Output {
        i.collect()
    }
}

#[doc(hidden)]
#[derive(Clone)]
pub struct iota<N> {
    pub from: N,
    pub to: Option<N>,
    pub closed: bool,
}

impl<N: AddAssign + Clone + PartialOrd> Iterator for iota<N>
where
    u8: cast<N>,
{
    type Item = N;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(to) = &self.to {
            let finished = if self.closed {
                self.from > *to
            } else {
                self.from >= *to
            };
            if finished {
                return None;
            }
        }
        let next = self.from.clone();
        self.from += 1.cast();
        Some(next)
    }
}
