use crate::{namespace, std};

namespace!(std = tupleh<Types> {

    /// ```
    /// # use rust__::{r#include, std};
    /// r#include!(<cassert>);
    /// r#include!(<tuple>);
    ///
    /// let t = (123, "hello", 87.0);
    /// std::assert(std::get::<0>(t) == 123);
    /// std::assert(std::get::<1>(t) == "hello");
    /// std::assert(std::get::<2>(t) == 87.0);
    /// ```
    fn get[const I: usize](t: Types) -> Types::Output
    where
        Types: tuple_get<I>
    {
        t.get()
    }

    type tuple_element<const I: usize, T: tuple_get<I>> = T::Output;
});

#[doc(hidden)]
pub trait tuple_get<const N: usize> {
    type Output;
    fn get(self) -> Self::Output;
}

macro_rules! tuples_get_impl {
    ( $firstmember:ident $firstindex:tt $(, $member:ident $index:tt)* $(
        [$($reversedmember:ident $reversedindex:tt),*]
        [$($sortedmember:ident $sortedindex:tt),*]
    )?) => {
        tuples_get_impl!( $($member $index),*
            [$firstmember $firstindex $($(,$reversedmember $reversedindex)*)?]
            [$($($sortedmember $sortedindex,)*)? $firstmember $firstindex]
        );
    };
    () => {};
    ( [$($member:ident $index:tt),*] [$lastmember:ident $lastindex:tt $(, $restmember:ident $restindex:tt)*] ) => {
        tuple_get_impl!( $($member $index),* ; $($member),* );
        tuples_get_impl!( $($restmember $restindex),* );
    };

}

macro_rules! tuple_get_impl {
    ( ; $($members:tt)* ) => {};
    ( $chosenmember:ident $chosenindex:tt $(,$member:ident $index:tt)* ; $($members:ident),*) => {
        impl <$($members),*> tuple_get<$chosenindex> for ($($members,)*) {
            type Output = $chosenmember;
            fn get(self) -> Self::Output {
                self.$chosenindex
            }
        }

        tuple_get_impl!( $($member $index),* ; $($members),* );
    };
}

tuples_get_impl!(L 11, K 10, J 9, I 8, H 7, G 6, F 5, E 4, D 3, C 2, B 1, A 0);
