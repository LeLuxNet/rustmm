use std::{
    io::{BufRead, ErrorKind, Read},
    ops::{Not, Shr},
    str::{self, FromStr},
};

use crate::{
    ios::{basic_ios, eofbit, failbit, goodbit, iostate},
    streamsize,
};

pub struct istream<R: Read> {
    pub(crate) r: R,
    pub(crate) b: iostate,
}

impl<R: Read> basic_ios for istream<R> {
    fn rdsstate(&self) -> iostate {
        self.b
    }
    fn clear(&mut self, state: iostate) {
        self.b = state;
    }
}
impl<R: Read> Not for istream<R> {
    type Output = bool;
    fn not(self) -> Self::Output {
        self.fail()
    }
}
impl<R: Read> Not for &istream<R> {
    type Output = bool;
    fn not(self) -> Self::Output {
        self.fail()
    }
}
impl<R: Read> From<istream<R>> for bool {
    fn from(value: istream<R>) -> Self {
        !value.fail()
    }
}

impl<R: Read> istream<R> {
    pub(crate) const fn new(r: R) -> Self {
        Self { r, b: goodbit }
    }

    pub fn get(&mut self) -> u8 {
        let mut b = [0];
        if self.r.read_exact(&mut b).is_err() {
            self.setstate(failbit | eofbit);
        }
        b[0]
    }
    pub fn read(&mut self, s: &mut [u8], count: streamsize) {
        if self.r.read_exact(&mut s[..*count as usize]).is_err() {
            self.setstate(failbit | eofbit);
        }
    }
}

impl<R: BufRead> istream<R> {
    pub fn readsome(&mut self, s: &mut [u8], count: streamsize) {
        if let Ok(b) = self.r.fill_buf() {
            let l = b.len().min(*count as usize);
            s[..l].copy_from_slice(&b[..l]);
        } else {
            self.setstate(eofbit);
        }
    }

    fn skip_until(&mut self, f: fn(u8) -> bool) -> bool {
        loop {
            let available = match self.r.fill_buf() {
                Ok(n) => n,
                Err(e) if e.kind() == ErrorKind::Interrupted => continue,
                Err(_) => return false,
            };
            let (done, used) = match available.iter().position(|b| f(*b)) {
                Some(i) => (true, i),
                None => (false, available.len()),
            };
            self.r.consume(used);
            if done || used == 0 {
                return true;
            }
        }
    }

    fn read_until(&mut self, f: fn(u8) -> bool, buf: &mut Vec<u8>) -> bool {
        loop {
            let available = match self.r.fill_buf() {
                Ok(n) => n,
                Err(e) if e.kind() == ErrorKind::Interrupted => continue,
                Err(_) => return false,
            };
            let (done, used) = match available.iter().position(|b| f(*b)) {
                Some(i) => {
                    buf.extend_from_slice(&available[..i]);
                    (true, i + 1)
                }
                None => {
                    buf.extend_from_slice(available);
                    (false, available.len())
                }
            };
            self.r.consume(used);
            if done || used == 0 {
                return true;
            }
        }
    }
}

impl<R: BufRead, V: FromStr + Default> Shr<&mut V> for &mut istream<R> {
    type Output = Self;

    fn shr(self, rhs: &mut V) -> Self::Output {
        if !self.skip_until(|b| !b.is_ascii_whitespace()) {
            return self;
        }

        let mut buf = Vec::new();
        if !self.read_until(|b| b.is_ascii_whitespace(), &mut buf) {
            return self;
        };

        let Ok(s) = str::from_utf8(&buf) else {
            return self;
        };

        if let Ok(r) = s.parse() {
            *rhs = r;
        } else {
            *rhs = V::default();
        }
        self
    }
}
impl<R: BufRead, V: FromStr + Default> Shr<&mut V> for istream<R> {
    type Output = Self;

    fn shr(mut self, rhs: &mut V) -> Self::Output {
        _ = &mut self >> rhs;
        self
    }
}
