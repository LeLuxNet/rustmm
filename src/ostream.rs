use ::std::{
    fmt::Display,
    io::Write,
    ops::{Not, Shl},
};

use crate::{
    ios::{badbit, basic_ios, goodbit, iostate},
    namespace, std, streamsize,
};

namespace!(std = ostreamh {
    const endl: char = '\n';
    const ends: char = '\0';

    const flush: flush = flush;
});

#[doc(hidden)]
pub struct flush;

impl<W: Write> Shl<flush> for &mut ostream<W> {
    type Output = Self;
    fn shl(self, _: flush) -> Self::Output {
        _ = self.w.flush();
        self
    }
}

impl<W: Write> Shl<flush> for ostream<W> {
    type Output = Self;
    fn shl(mut self, _: flush) -> Self::Output {
        _ = self.w.flush();
        self
    }
}

pub struct ostream<W> {
    pub(crate) w: W,
    pub(crate) b: iostate,
}

impl<W> basic_ios for ostream<W> {
    fn rdsstate(&self) -> iostate {
        self.b
    }
    fn clear(&mut self, state: iostate) {
        self.b = state;
    }
}

impl<W> Not for ostream<W> {
    type Output = bool;
    fn not(self) -> Self::Output {
        self.fail()
    }
}
impl<W> Not for &ostream<W> {
    type Output = bool;
    fn not(self) -> Self::Output {
        self.fail()
    }
}

impl<W> From<ostream<W>> for bool {
    fn from(value: ostream<W>) -> Self {
        !value.fail()
    }
}
impl<W> From<&ostream<W>> for bool {
    fn from(value: &ostream<W>) -> Self {
        !value.fail()
    }
}

impl<W: Write> ostream<W> {
    pub(crate) const fn new(w: W) -> Self {
        Self { w, b: goodbit }
    }

    pub fn put(&mut self, ch: u8) {
        if self.w.write(&[ch]).is_err() {
            self.setstate(badbit);
        }
    }
    pub fn write(&mut self, s: &[u8], count: streamsize) {
        if self.w.write(&s[..*count as usize]).is_err() {
            self.setstate(badbit);
        }
    }
    pub fn flush(&mut self) {
        _ = self.w.flush();
    }
}

impl<T: Display, W: Write> Shl<T> for &mut ostream<W> {
    type Output = Self;
    fn shl(self, rhs: T) -> Self::Output {
        _ = write!(self.w, "{rhs}");
        self
    }
}

impl<T: Display, W: Write> Shl<T> for ostream<W> {
    type Output = Self;
    fn shl(mut self, rhs: T) -> Self::Output {
        _ = &mut self << rhs;
        self
    }
}
