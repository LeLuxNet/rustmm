use crate::{namespace, std};

namespace!(std = exceptionh {});

pub trait exception {
    fn what(&self) -> &str;
}
