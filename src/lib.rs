#![allow(non_camel_case_types, non_upper_case_globals)]

use ::std::collections::{hash_map::RandomState, BTreeMap, BTreeSet, HashMap, HashSet};

pub struct std;
pub use filesystem::fs::filesystem as fs;
#[doc(hidden)]
pub use ranges::views::views;

mod throw;
pub use throw::*;

mod istreams;
pub use istreams::*;

mod header;

mod numbers;
pub use numbers::*;

pub mod cassert;
pub mod cctype;
pub mod cmath;
pub mod cstdlib;
pub mod exception;
pub mod filesystem;
pub mod fstream;
pub mod ios;
pub mod iostream;
pub mod iterator;
pub mod ostream;
pub mod ranges;
pub mod regex;
pub mod sstream;
pub mod stdexcept;
pub mod string;
pub mod tuple;
pub mod type_info;
pub mod vector;

pub type array<T, const N: usize> = [T; N];
pub type map<K, V> = BTreeMap<K, V>;
pub type set<K> = BTreeSet<K>;
pub type unordered_map<K, V, H = RandomState> = HashMap<K, V, H>;
pub type unordered_set<K, H = RandomState> = HashSet<K, H>;

/// Compute the size of a type in bytes
/// ```
/// # use rust__::{r#include, sizeof, std, char, int16_t, int32_t, double, array};
/// r#include!(<cassert>);
///
/// std::assert(sizeof!(char) == 1);
/// std::assert(sizeof!(int32_t) == 4);
/// std::assert(sizeof!(double) == 8);
/// std::assert(sizeof!(array<int16_t, 16>) == 32);
/// ```
#[macro_export]
macro_rules! sizeof {
    ($type:path) => {
        ($crate::size_t {
            n: ::std::mem::size_of::<$type>(),
        })
    };
}

/// Compute the alignment of a type in bytes
/// ```
/// # use rust__::{r#include, alignof, std, char, int16_t, int32_t, double, array};
/// r#include!(<cassert>);
///
/// std::assert(alignof!(char) == 1);
/// std::assert(alignof!(int32_t) == 4);
/// std::assert(alignof!(double) == 8);
/// std::assert(alignof!(array<int16_t, 16>) == 2);
/// ```
#[macro_export]
macro_rules! alignof {
    ($type:path) => {
        ($crate::size_t {
            n: ::std::mem::align_of::<$type>(),
        })
    };
}

/// ```
/// # use rust__::{r#include, std, type_id, int, float};
/// r#include!(<cassert>);
/// r#include!(<type_info>);
///
/// std::assert(type_id!(int) == std::type_info::<int>());
/// std::assert(type_id!(true) == std::type_info::<bool>());
/// ```
#[macro_export]
macro_rules! type_id {
    ($type:ty) => {
        (<$crate::std as $crate::type_info::h>::type_info::<$type>())
    };
    ($expression:expr) => {{
        fn check<T: ?Sized + 'static>(_: &T) -> $crate::type_info::type_info {
            <$crate::std as $crate::type_info::h>::type_info::<T>()
        }
        check(&($expression))
    }};
}

#[doc(hidden)]
#[macro_export]
macro_rules! switch_arm {
    ($ff:ident, $var:ident,) => {};
    ($ff:ident, $var:ident, default; $($tt:tt)*) => {
        $crate::switch_arm!(true, $var, $($tt)*)
    };
    ($ff:ident, $var:ident, case $expr:expr; $($tt:tt)*) => {
        let _ff = $ff || $var == $expr;
        $crate::switch_arm!(_ff, $var, $($tt)*)
    };
    ($ff:ident, $var:ident, $stmt:stmt; $($tt:tt)*) => {
        if $ff {
            {$stmt};
        }
        $crate::switch_arm!($ff, $var, $($tt)*)
    };
}

/// Switch statements with fallthrough and break semantics
/// ```
/// # use rust__::{r#include, std, switch};
/// r#include!(<iostream>);
///
/// switch! { (5)
///     case 0;
///     case 1;
///     case 2;
///     case 3;
///     case 4;
///         std::cout << "monday - friday" << std::endl;
///         break;
///     case 5;
///         std::cout << "saturday" << std::endl;
///     default;
///         std::cout << "weekend!" << std::endl;
/// }
/// ```
/// will output
/// ```text
/// saturday
/// weekend!
/// ```
#[macro_export]
macro_rules! switch {
    (($var:expr) $($tt:tt)*) => {{
        loop {
            use $crate::switch_arm;
            let _var = ($var);
            switch_arm!(false, _var, $($tt)* );
            break;
        };
    }};
}

/// Include headers and load their functions into the namespace
/// ```compile_fail
/// use rust__::{r#include, std};
///
/// std::sqrt(9);
/// ```
///
/// ```
/// use rust__::{r#include, std};
///
/// r#include!(<cmath>);
///
/// std::sqrt(9);
/// ```
#[macro_export]
macro_rules! r#include {
    (< $($header:ident)/+ $(.h)? >) => {
        #[allow(unused_imports)]
        pub use $crate::$($header)::+::h as _;
        #[allow(unused_imports)]
        pub use $crate::$($header)::+::_hinclude::*;
    };
}
