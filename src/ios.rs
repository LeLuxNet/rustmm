use std::fs::OpenOptions;

#[cfg(unix)]
use std::os::unix::fs::OpenOptionsExt;

#[cfg(target_os = "windows")]
use std::os::windows::fs::OpenOptionsExt;

#[cfg(any(unix, target_os = "windows"))]
use libc::O_EXCL;

pub type iostate = u8;

pub const goodbit: iostate = 0;
pub const badbit: iostate = 1;
pub const failbit: iostate = 2;
pub const eofbit: iostate = 4;

pub trait basic_ios {
    fn rdsstate(&self) -> iostate;
    fn setstate(&mut self, state: iostate) {
        self.clear(self.rdsstate() | state)
    }
    fn clear(&mut self, state: iostate);

    fn good(&self) -> bool {
        self.rdsstate() == goodbit
    }
    fn fail(&self) -> bool {
        self.rdsstate() & (failbit | badbit) != 0
    }
    fn bad(&self) -> bool {
        self.rdsstate() & badbit != 0
    }
    fn eof(&self) -> bool {
        self.rdsstate() & eofbit != 0
    }
}

pub type openmode = u8;

pub const app: openmode = 1;
pub const binary: openmode = 2;
pub const r#in: openmode = 4;
pub const out: openmode = 8;
pub const trunc: openmode = 16;
pub const ate: openmode = 32;
pub const noreplace: openmode = 64;

pub(crate) fn openmode_options(mode: openmode) -> OpenOptions {
    let mut o = OpenOptions::new();
    o.append(mode & app != 0);
    o.read(mode & r#in != 0);
    o.write(mode & out != 0);
    o.truncate(mode & trunc != 0);
    o.create(mode & (out | app) != 0);
    // if mode & ate != 0 {} // TODO

    #[cfg(any(unix, target_os = "windows"))]
    if mode & noreplace != 0 {
        o.custom_flags(O_EXCL);
    }

    o
}

#[cfg(test)]
mod tests {
    use crate::{fs, ios::*, r#include, std};
    macro_rules! include { ($($t:tt)*) => { $crate::include!($($t)*); }; }

    r#include!(<filesystem>);
    r#include!(<fstream>);

    #[test]
    fn open_exclusive() {
        let p = fs::temp_directory_path() / "exclusive_test.txt";
        fs::remove(&p);

        let mut f = std::ofstream(&p, out | trunc | noreplace);
        assert!(f.good());
        f.close();

        let mut f = std::ofstream(&p, out | trunc | noreplace);
        assert!(f.fail());
        f.close();
    }
}
