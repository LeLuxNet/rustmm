use ::std::ops::{Index, IndexMut};

use crate::{namespace, size_t, std};

namespace!(std = vectorh {
    type vector<T> = vector<T>;
    fn vector[T]() -> vector<T> {
        vector { v: Vec::new() }
    }
});

#[doc(hidden)]
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct vector<T> {
    v: Vec<T>,
}

impl<T> vector<T> {
    pub fn at(&self, pos: size_t) -> &T {
        self.v.get(*pos).unwrap()
    }

    pub fn front(&self) -> &T {
        self.v.first().unwrap()
    }

    pub fn back(&self) -> &T {
        self.v.last().unwrap()
    }

    pub fn size(&self) -> size_t {
        (size_t)(self.v.len())
    }

    pub const fn max_size(&self) -> size_t {
        let max = usize::MAX / size_of::<T>() - 1;
        size_t { n: max }
    }

    pub fn empty(&self) -> bool {
        self.v.is_empty()
    }

    pub fn capacity(&self) -> size_t {
        (size_t)(self.v.capacity())
    }

    pub fn reserve(&mut self, n: size_t) {
        if let Some(additional) = n.checked_sub(self.v.len()) {
            self.v.reserve_exact(additional);
        }
    }

    pub fn shrink_to_fit(&mut self) {
        self.v.shrink_to_fit();
    }

    pub fn push_back(&mut self, value: T) {
        self.v.push(value)
    }

    pub fn clear(&mut self) {
        self.v.clear();
    }
}

impl<T> Index<size_t> for vector<T> {
    type Output = T;
    fn index(&self, index: size_t) -> &Self::Output {
        self.at(index)
    }
}

impl<T> IndexMut<size_t> for vector<T> {
    fn index_mut(&mut self, index: size_t) -> &mut Self::Output {
        self.v.get_mut(*index).unwrap()
    }
}

impl<T> FromIterator<T> for vector<T> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        Self {
            v: iter.into_iter().collect(),
        }
    }
}
