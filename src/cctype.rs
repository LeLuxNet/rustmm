use crate::{cast, int, namespace, std, unsigned_char};

namespace!(std = cctypeh {
    fn isspace(ch: impl cast<unsigned_char>) -> int {
        matches!(ch.cast().n, b'\t' | b'\n').cast()
    }
});
