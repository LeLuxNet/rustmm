use ::std::ffi::CStr;
use core::str;

use regress::Regex;

use crate::{
    exception::exception, namespace, std, stdexcept::runtime_error, string::string, throw,
};

namespace!(std = regexh {
    type regex = regex;

    /// ```
    /// # use rust__::{r#include, std};
    /// r#include!(<regex>);
    ///
    /// std::regex("[a-z]+");
    /// ```
    ///
    /// ```should_panic
    /// # use rust__::{r#include, std};
    /// # r#include!(<regex>);
    /// std::regex("[a-z");
    /// ```
    fn regex(s: &str) -> regex {
        match Regex::new(s) {
            Ok(r) => regex { r },
            Err(e) => throw(regex_error{ what: e.to_string() }),
        }
    }

    /// ```
    /// # use rust__::{r#include, std};
    /// r#include!(<cassert>);
    /// r#include!(<regex>);
    ///
    /// let r = std::regex("[A-Z]\\d");
    /// std::assert(std::regex_match("A3", &r) == true);
    /// std::assert(std::regex_match("xyB8mn", &r) == true);
    /// std::assert(std::regex_match("abc", &r) == false);
    /// ```
    fn regex_match(s: impl chars, r: &regex) -> bool {
        r.r.find(s.as_str()).is_some()
    }

    type regex_error = regex_error;
});

#[doc(hidden)]
pub struct regex {
    r: Regex,
}

#[doc(hidden)]
pub struct regex_error {
    what: String,
}

impl exception for regex_error {
    fn what(&self) -> &str {
        &self.what
    }
}

impl runtime_error for regex_error {}

#[doc(hidden)]
pub trait chars {
    fn as_str(&self) -> &str;
}

impl chars for &str {
    fn as_str(&self) -> &str {
        self
    }
}

impl chars for &CStr {
    fn as_str(&self) -> &str {
        str::from_utf8(self.to_bytes()).unwrap()
    }
}

impl chars for &string {
    fn as_str(&self) -> &str {
        let b = &self.data()[..self.size().n];
        str::from_utf8(b).unwrap()
    }
}
