use ::std::{
    fs::File,
    io::{self, BufRead, BufReader, Read, Write},
    path::Path,
};

use crate::{
    ios::{basic_ios, failbit, goodbit, openmode, openmode_options},
    istream, namespace,
    ostream::ostream,
    std,
};

namespace!(std = fstreamh {
    fn ifstream[P: AsRef<Path>](p: P, mode: openmode) -> istream<file<BufReader<File>>> {
        let mut o = istream::new(file::Closed);
        o.open(p, mode);
        o
    }

    fn ofstream[P: AsRef<Path>](p: P, mode: openmode) -> ostream<file<File>> {
        let mut o = ostream::new(file::Closed);
        o.open(p, mode);
        o
    }
});

#[doc(hidden)]
pub enum file<F> {
    Open(F),
    Closed,
}

impl<F: Read> Read for file<F> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        match self {
            Self::Open(f) => f.read(buf),
            Self::Closed => Ok(0),
        }
    }
}

impl<F: BufRead> BufRead for file<F> {
    fn fill_buf(&mut self) -> io::Result<&[u8]> {
        match self {
            Self::Open(f) => f.fill_buf(),
            Self::Closed => Ok(&[]),
        }
    }

    fn consume(&mut self, amt: usize) {
        match self {
            Self::Open(f) => f.consume(amt),
            Self::Closed => {}
        }
    }
}

impl istream<file<BufReader<File>>> {
    pub fn is_open(&self) -> bool {
        matches!(self.r, file::Open(_))
    }

    pub fn open<P: AsRef<Path>>(&mut self, p: P, mode: openmode) {
        match openmode_options(mode).open(&p) {
            Ok(f) => {
                self.r = file::Open(BufReader::new(f));
                self.b = goodbit;
            }
            Err(_) => {
                self.r = file::Closed;
                self.setstate(failbit);
            }
        }
    }

    pub fn close(&mut self) {
        self.r = file::Closed;
    }
}

impl<F: Write> Write for file<F> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        match self {
            Self::Open(f) => f.write(buf),
            Self::Closed => Ok(buf.len()),
        }
    }

    fn flush(&mut self) -> io::Result<()> {
        match self {
            Self::Open(f) => f.flush(),
            Self::Closed => Ok(()),
        }
    }
}

impl ostream<file<File>> {
    pub fn is_open(&self) -> bool {
        matches!(self.w, file::Open(_))
    }

    pub fn open<P: AsRef<Path>>(&mut self, p: P, mode: openmode) {
        match openmode_options(mode).open(&p) {
            Ok(f) => {
                self.w = file::Open(f);
                self.b = goodbit;
            }
            Err(_) => {
                self.w = file::Closed;
                self.setstate(failbit);
            }
        }
    }

    pub fn close(&mut self) {
        self.w = file::Closed;
    }
}
