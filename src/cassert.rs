use ::std::process::abort;

use crate::{namespace, std};

namespace!(std = casserth {

    /// ```
    /// # use rust__::{r#include, std};
    /// r#include!(<cassert>);
    ///
    /// std::assert(2 + 2 == 4);
    /// ```
    ///
    /// ```should_panic
    /// # use rust__::{r#include, std};
    /// r#include!(<cassert>);
    ///
    /// std::assert(3 + 1 == 3);
    /// ```
    fn assert(b: bool) {
        if !b {
            eprintln!("Assertion '{b}' failed.");
            abort();
        }
    }
});
