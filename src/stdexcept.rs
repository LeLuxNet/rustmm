use crate::{exception::exception, namespace, std};

namespace!(std = stdexcepth {
    fn out_of_range(what: String) -> out_of_range {
        out_of_range { what }
    }
});

pub trait runtime_error: exception {}

pub trait logic_error: exception {}

pub struct out_of_range {
    pub(crate) what: String,
}

impl exception for out_of_range {
    fn what(&self) -> &str {
        &self.what
    }
}

impl logic_error for out_of_range {}
