use ::std::ops::{BitOr, Deref};

use crate::{namespace, std};

pub mod views;

namespace!(std: <ranges/views> = rangesh {
    type views = views::views;
});

pub struct range<I>(pub I);

impl<I: Iterator> Iterator for range<I> {
    type Item = I::Item;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}

impl<I> Deref for range<I> {
    type Target = I;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<I> range<I>
where
    I: Iterator,
{
    pub fn begin(&mut self) -> I::Item {
        self.0.next().unwrap()
    }
}

pub trait adaptor<I> {
    type Output;
    fn pipe(self, i: I) -> Self::Output;
}

impl<I, V> BitOr<V> for range<I>
where
    V: adaptor<I>,
{
    type Output = V::Output;
    fn bitor(self, rhs: V) -> Self::Output {
        rhs.pipe(self.0)
    }
}
