#[macro_export]
macro_rules! namespace {
    (
        $namespace:path
        $(:$( <$($include:tt)/+> )+)?
        = $name:ident
        $(<$($impltypes:ident),+>)?
        $([$($trait:tt)*] [$($impl:tt)*])?
        {}
    ) => {
        mod _h {
            #[allow(unused_imports)]
            use super::*;

            pub trait $name $(<$($impltypes),+>)? {
                $($($trait)*)?
            }
        }
        pub use _h::$name as h;

        #[doc(hidden)]
        pub mod _hinclude {
            $($( $crate::r#include!(<$($include)/+>); )+)?
        }

        impl $(<$($impltypes),+>)? h $(<$($impltypes),+>)?  for $namespace {
            $($($impl)*)?
        }
    };
    (
        $namespace:path
        $(:$( <$($include:tt)/+> )+)?
        $(= <$($impltypes:ident),+>)?
        $([$($trait:tt)*] [$($impl:tt)*])?
        {}
    ) => {
        pub trait h {
            $($($trait)*)?
        }

        #[doc(hidden)]
        pub mod _hinclude {
            $($( $crate::r#include!(<$($include)/+>); )+)?
        }

        impl $(<$($impltypes),+>)? h $(<$($impltypes),+>)? for $namespace {
            $($($impl)*)?
        }
    };
    (
        $namespace:path
        $(: $(<$($include:ident)/+>)+ )?
        $(= $($name:ident)? $(<$($impltypes:ident),+>)? )?
        $([$($trait:tt)*] [$($impl:tt)*])?
        { $(#[$($attr:tt)+])* fn $fname:ident $([$($generic:tt)+])? ($($params:tt)*) $(-> $ret:ty)? $(where $($wherei:path : $wheret:path ),+ )? $body:block $($rest:tt)* }
    ) => {
        $crate::namespace!{
            $namespace
            $(: $(<$($include)/+>)+ )?
            $(= $($name)? $(<$($impltypes),+>)? )?
            [ $($($trait)*)? $(#[$($attr)+])* fn $fname $(<$($generic)+>)? ($($params)*) $(-> $ret)? $(where $($wherei: $wheret),+)? ; ]
            [ $($($impl)*)?  $(#[$($attr)+])* fn $fname $(<$($generic)+>)? ($($params)*) $(-> $ret)? $(where $($wherei: $wheret),+)? $body ]
            { $($rest)* }
        }
    };
    (
        $namespace:path
        $(: $(<$($include:ident)/+>)+ )?
        $(= $($name:ident)? $(<$($impltypes:ident),+>)? )?
        $([$($trait:tt)*] [$($impl:tt)*])?
        { $(#[$($attr:tt)+])* class $cname:ident $(<$( $gname:ident $(: $gtype:path)? ),+>)? ($($paramn:ident: $paramt:ty)*); $($rest:tt)* }
    ) => {
        $crate::namespace!{
            $namespace
            $(: $(<$($include)/+>)+ )?
            $(= $($name)? $(<$($impltypes),+>)? )?
            [ $($($trait)*)? $(#[$($attr)+])* fn $cname $(<$( $gname $(: $gtype)? ),+>)? ($($paramn: $paramt),*) -> $cname$(<$($gname)*>)? ;                        ]
            [ $($($impl)*)?  $(#[$($attr)+])* fn $cname $(<$( $gname $(: $gtype)? ),+>)? ($($paramn: $paramt),*) -> $cname$(<$($gname)*>)? { $cname($($paramn),*) } ]
            { $($rest)* }
        }
    };
    (
        $namespace:path
        $(: $(<$($include:ident)/+>)+ )?
        $(= $($name:ident)? $(<$($impltypes:ident),+>)? )?
        $([$($trait:tt)*] [$($impl:tt)*])?
        { $(#[$($attr:tt)+])* type $tname:ident $(<$( $($gnames:ident)* $(: $gtype:path)? ),+>)? = $ttype:ty; $($rest:tt)* }
    ) => {
        $crate::namespace!{
            $namespace
            $(: $(<$($include)/+>)+ )?
            $(= $($name)? $(<$($impltypes),+>)? )?
            [ $($($trait)*)? $(#[$($attr)+])* type $tname $(<$( $($gnames)* $(: $gtype)? ),+>)?         ; ]
            [ $($($impl)*)?  $(#[$($attr)+])* type $tname $(<$( $($gnames)* $(: $gtype)? ),+>)? = $ttype; ]
            { $($rest)* }
        }
    };
    (
        $namespace:path
        $(: $(<$($include:ident)/+>)+ )?
        $(= $($name:ident)? $(<$($impltypes:ident),+>)? )?
        $([$($trait:tt)*] [$($impl:tt)*])?
        { $(#[$($attr:tt)+])* type $tname:ident: $ttype:ty; $($rest:tt)* }
    ) => {
        $crate::namespace!{
            $namespace
            $(: $(<$($include)/+>)+ )?
            $(= $($name)? $(<$($impltypes),+>)? )?
            [ $($($trait)*)? $(#[$($attr)+])* type $tname: $ttype; ]
            [ $($($impl)*)?                                        ]
            { $($rest)* }
        }
    };
    (
        $namespace:path
        $(: $(<$($include:ident)/+>)+ )?
        $(= $($name:ident)? $(<$($impltypes:ident),+>)? )?
        $([$($trait:tt)*] [$($impl:tt)*])?
        { $(#[$($attr:tt)+])* const $cname:ident : $ctype:ty = $cval:expr; $($rest:tt)* }
    ) => {
        $crate::namespace!{
            $namespace
            $(: $(<$($include)/+>)+ )?
            $(= $($name)? $(<$($impltypes),+>)? )?
            [ $($($trait)*)? $(#[$($attr)+])* const $cname: $ctype        ; ]
            [ $($($impl)*)?  $(#[$($attr)+])* const $cname: $ctype = $cval; ]
            { $($rest)* }
        }
    };
}
